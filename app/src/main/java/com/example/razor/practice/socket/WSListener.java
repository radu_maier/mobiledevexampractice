package com.example.razor.practice.socket;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * Created by razor on 1/28/2017.
 */

public class WSListener extends WebSocketListener {

    //start the server, override all methods and put breakpoitns

    @Override public void onOpen(WebSocket webSocket, Response response) {
        System.out.println("onOpen ---------------------- ");
    }

//    @Override public void onOpen(WebSocket webSocket, Response response) {
//        webSocket.send("Hello...");
//        webSocket.send("...World!");
//        webSocket.send(ByteString.decodeHex("deadbeef"));
//        webSocket.close(1000, "Goodbye, World!");
//    }

    @Override public void onMessage(WebSocket webSocket, String text) {
        System.out.println("MESSAGE: " + text);
    }

    @Override public void onMessage(WebSocket webSocket, ByteString bytes) {
        System.out.println("MESSAGE: " + bytes.hex());
    }

    @Override public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(1000, null);
        System.out.println("CLOSE: " + code + " " + reason);
    }

    @Override public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t.printStackTrace();
    }
}

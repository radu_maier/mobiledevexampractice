package com.example.razor.practice;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.razor.practice.socket.WSListener;
import com.example.razor.practice.utils.ConnectionHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class MainActivity extends AppCompatActivity {

    private final OkHttpClient client = new OkHttpClient();
    private final String _url = "http://192.168.0.106:3000"; // MUST BE ON WI-FI

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private static String _lastModified = "";

    Handler _handler=  new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!ConnectionHelper.isConn(this)) {
            Button sendPostBtn = (Button) findViewById(R.id.send_post);
            sendPostBtn.setEnabled(false);
            return;
        }

        Request request = new Request.Builder()
                .url(_url+"/books")
                .build();

        OkHttpClient myclient = new OkHttpClient.Builder()
                .connectTimeout(10,TimeUnit.SECONDS)
                .readTimeout(10,  TimeUnit.SECONDS)
                .writeTimeout(10,TimeUnit.SECONDS)
                .build();

        WebSocket ws = myclient.newWebSocket(request, new WSListener());

        // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
        myclient.dispatcher().executorService().shutdown();
    }//on create

    public void sendPost(View view){
        if(!ConnectionHelper.isConn(this))
            return;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title","Alice");
            //jsonObject.put("x","Alice");
            //jsonObject.put("y","Alice");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String json = jsonObject.toString();
        RequestBody body = RequestBody.create(JSON, json);
        //body = requesbody
        Request request = new Request.Builder()
                .url(_url+"/book")
                .post(body)
                .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code at post " + response);
                    if(response.code()==201){
                        System.out.println("POST responded with 201");
                    }
                }
            });
    }

    public void sendGet(View view){
        if(!ConnectionHelper.isConn(this))
            return;

        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        Request.Builder builder = new Request.Builder()
                .url(_url + "/books");
        //.addHeader("Accept", "application/json")
        if(!_lastModified.isEmpty()){
            builder.addHeader("If-Modified-Since",_lastModified);
        }
        Request request = builder.build();

        client.newCall(request).enqueue(new Callback() {
            @Override public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                Headers responseHeaders = response.headers();
                _lastModified = responseHeaders.get("Last-Modified");
                for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                    System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
                }

                final int NOT_MODIFIED = 304;
                if(response.code() == NOT_MODIFIED ){
                    return;
                }

                try {
                    String jsonData = response.body().string();

                    JSONArray array = new JSONArray(jsonData);
                    //JSONObject Jobject = new JSONObject(jsonData);

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object     = array.getJSONObject(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                _handler.post(new Runnable() { // runs on the thread it was created, in this case Main
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

            }
        });

    }
}//class
